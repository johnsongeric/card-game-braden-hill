#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Suit
{
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES
};

enum Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card &card)
{
	cout << "The ";
	switch (card.rank)
	{
		case 2: cout << "Two"; break;
		case 3: cout << "Three"; break;
		case 4: cout << "Four"; break;
		case 5: cout << "Five"; break;
		case 6: cout << "Six"; break;
		case 7: cout << "Seven"; break;
		case 8: cout << "Eight"; break;
		case 9: cout << "Nine"; break;
		case 10: cout << "Ten"; break;
		case 11: cout << "Jack"; break;
		case 12: cout << "Queen"; break;
		case 13: cout << "King"; break;
		case 14: cout << "Ace"; break;
		default: break;
	}
	cout << " of ";
	switch (card.suit)
	{
		case 0: cout << "Clubs "; break;
		case 1: cout << "Diamonds "; break;
		case 2: cout << "Hearts "; break;
		case 3: cout << "Spades "; break;
		default: break;
	}
	cout << "\n";
}

Card HighCard(Card &card1, Card &card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	if (card2.rank > card1.rank)
	{
		return card2;
	}
	// If code gets here cards are likely the same rank
	return card1;
}


int main()
{
	// Define Stuff
	Card cardA;
	Card cardB;
	Card cardC;

	Card HighC;
	// Assign Cards

	cardA.suit = HEARTS;
	cardA.rank = QUEEN;

	cardB.suit = DIAMONDS;
	cardB.rank = TWO;

	cardC.suit = SPADES;
	cardC.rank = ACE;

	PrintCard(cardA);
	PrintCard(cardB);
	PrintCard(cardC);

	cout << "\n The high card is the ";
	HighC = HighCard(cardA, cardB);
	PrintCard (HighC);

	cout << "\n The high card is the ";
	HighC = HighCard(cardB, cardC);
	PrintCard(HighC);

	_getch();
	return 0;
}
